# encoding: UTF-8
class UpdateBlankVersionsOnSurveys < ActiveRecord::Migration
  class Survey < ActiveRecord::Base; end

  def self.up
    Survey.where('survey_version IS ?', nil).each do |s|
      s.survey_version = 0
      s.save!
    end
  end

  def self.down
  end
end
