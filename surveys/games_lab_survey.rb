# encoding: UTF-8
survey "Games Lab survey", :default_mandatory => false do

  section "Developer Details" do
    # Explain the Survey
    label "This suvery will take approxiatemately 20-30 minutes to complete."
    label "The following questions are to gauge your experience as a Developer/Designer, so please answer them as accurately as possible."

    # Name
    question "What is your name?"
    answer :string

    # Age
    question "What age bracket are within?", :pick => :one
    answer "<= 17"
    answer "18-20"
    answer "21-25"
    answer "26-30"
    answer ">= 31"

    # Course details
    question "What degree are currently undertaking at Swinburne?"
    answer :string

    # Programming experience
    question_role "Are you a Programmer or Designer?", :pick => :one
    answer_programmer "Programmer"
    answer_designer "Designer"

    question "How many years of experience programming?", :pick => :one, :display_type => :slider
    (0..9).to_a.each{|num| a num.to_s}

    # Programming languages Grid
    grid "What level of experience do have in the following programming languages?" do
      dependency :rule => "A"
      condition_A :question_role, "==", :answer_programmer
      answer "None"
      answer "A Little"
      answer "Moderate"
      answer "Competent"
      answer "Expert"
      question "ActionScript", :pick => :one
      question "Boo", :pick => :one
      question "C/C++", :pick => :one
      question "C#", :pick => :one
      question "Java", :pick => :one
      question "JavaScript", :pick => :one
      question "Lua", :pick => :one
      question "Python", :pick => :one
      question "Ruby", :pick => :one
      question "Shell Script (Bash)", :pick => :one
      question "UnityScript", :pick => :one
    end

    question "What Intergrated Developer Environment (IDE) do you use?", :pick => :any
    answer "Code::Blocks"
    answer "Eclipse"
    answer "Emacs"
    answer "IntelliJ IDEA"
    answer "Microsoft Visual Studio"
    answer "MonoDevelop"
    answer "Quincy"
    answer "Sublime Text"
    answer "TextMate"
    answer "Vim"
    answer "Xcode"
    dependency :rule => "A"
    condition_A :question_role, "==", :answer_programmer

    # Game Engines Grid
    grid "What level experience do have with the following Game Engines/Frameworks?" do
      answer "None"
      answer "A Little"
      answer "Moderate"
      answer "Competent"
      answer "Expert"
      question "Cocos2d-x", :pick => :one
      question "Construct 2", :pick => :one
      question "CryEngine", :pick => :one
      question "GameMaker: Studio", :pick => :one
      question "Simple DirectMedia Layer (SDL)", :pick => :one
      question "Simple and Fast Multimedia Library (SFML)", :pick => :one
      question "Source SDK", :pick => :one
      question "Unreal Development Kit (UDK)", :pick => :one
      question "Unity3D", :pick => :one
    end

    # Developer Tools Grid
    grid "What level experience do have with the following developer tools?" do
      answer "None"
      answer "A Little"
      answer "Moderate"
      answer "Competent"
      answer "Expert"
      question "Adobe Photoshop", :pick => :one
      question "Adobe Flash", :pick => :one
      question "Adobe Illustrator", :pick => :one
      question "Autodesk 3D Studio Max", :pick => :one
      question "Autodesk Maya", :pick => :one
      question "Blender", :pick => :one
      question "MudBox", :pick => :one
      question "ZBrush", :pick => :one
    end

    # software development practices (TDD, BDD, testing)
    question_sp "Do you practice any of the following Software Philosophies", :pick => :any
    answer "Continuous Integration"
    answer "Best Practice"
    answer "Desgin by Contract"
    answer "Don't Repeat Yourself (DRY)"
    answer "Keep It Simple Stupid (KISS) principle"
    answer "Inversion of control"
    answer "Planning Poker"
    answer "Refactoring"
    answer_oo "Object-Oriented Programming"

    question "O, rly? Please explain Object-Oriented Programming.", :pick => :any
    answer :text
    dependency :rule => "A"
    condition_A :question_sp, "==", :answer_oo

    question "Do you practice any of the following Software Methodologies?", :pick => :any
    answer "Agile"
    answer "Extreme Programming (XP)"
    answer "Kanban (development)"
    answer "Lean software development"
    answer "Pair Programming"
    answer "SCRUM"
    answer "Spiral model"
    answer "Waterfall model"

    question "Do you practice any of the following Software Development Processes", :pick => :any
    answer "Behavior-driven development (BDD)"
    answer "Design-driven development (D3)"
    answer "Domain-Driven Design (DDD)"
    answer "Feature Driven Development (FDD)"
    answer "Test-driven development (TDD)"
    answer "User-centered design (UCD)"

  end

  # This section is a series of questions about the team and the final year
  # project. This is to get a perspective of the teams' current project
  # management practice and capture what the original process is so that it can
  # be to later. Ensuring that improvements made are after using CI are becuase
  # of using CI and not just an improvement elsewhere.
  section "Project Team Process" do

    label "The following questions about the team and the final year project."

    question "How long do you spend each week coding or creating new assets?", :pick => :one
    answer "1 hour"
    answer "2 hours"
    answer "4 hours"
    answer "8 hours"
    answer "1 day"
    answer "2 days"
    answer "4 days"
    answer "a week or more"

    question "Briefly describe how you currently collect and merge individual work together"
    answer :text

    question "Is work reviewed before it is merged?", :pick => :one
    answer_y "Yes"
    answer_n "No"

    question "How long does it usually take to merge people's work together?", :pick => :one
    answer "1 hour"
    answer "2 hours"
    answer "4 hours"
    answer "8 hours"
    answer "1 day"
    answer "2 days"
    answer "4 days"
    answer "a week or more"
    answer "Cannot be merged"

    question "How are you usually involved with merging work? The person:", :pick => :any
    answer "Merging the work together"
    answer "Providing the work to merge"
    answer "Waiting for work to be merged"
    answer "Review the work before it is merged"
    answer "Review the work after it is merged"

    question "What tools are you using to assist with merging work together?", :pick => :any
    answer "BitBucket"
    answer "File Explorer"
    answer "Facebook"
    answer "DropBox"
    answer "E-mail"
    answer "GitHub"
    answer "Git"
    answer "Google Drive"
    answer "Slack"
    answer "SourceTree"
    answer "Trello"
    answer "USB stick or harddrive"

    question "What improvements do you think can made to the process of merging work"
    answer :text

  end

  section "Continuous Integration Experience" do

    label "The following questions are to gauge your experience and understanding of Continuous Integration."

    question_1 "Have you ever used a Continuous Integration service before?", :pick => :one
    answer_y "Yes"
    answer_n "No"

    question_1a "Please explain your experience with Continuous Integration?"
    answer_1 "explanation", :text, :help_text => "Please describe the system and how you interacted with it"
    dependency :rule => "A"
    condition_A :question_1, "==", :answer_y

    # Expectations
    question_2 "What do you think Continuous Integration can do?"
    answer_2 :text

    question_3 "What would you like Continuous Integration to do"
    answer_3 :text

    label "Thank you for time and for completing this survey."
  end
end
